
package main

import (
  "net/http"
)

func FetchUrl(r *http.Request) (string, *Error) {
  urls, ok := r.Form["url"]
  if ok == false || len(urls) != 1 {
    return "", &Error{
      M_malformatedQuery_url,
      400,
    }
  }
  return urls[0], nil
}

