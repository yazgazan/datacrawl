
package main

type Res interface{}

type ResError struct{
  Reason string `json:"reason"`
}

type ResCrawl struct{
  Url string `json:"url"`
  Results map[string]interface{} `json:"results"`
}

type ResTest struct{
  Test string `json:"test"`
}

