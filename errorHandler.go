
package main

import (
  "net/http"
  "encoding/json"
)

type Error struct {
  What string
  Code int
}

func (e Error) Error() string {
  return e.What
}

type HandlerFunc func(http.ResponseWriter, *http.Request) (Res, *Error)

func errorHandler(f HandlerFunc) http.HandlerFunc {
  return func(w http.ResponseWriter, r *http.Request) {
    res, err := f(w, r)
    if err != nil {
      reserror := ResError{err.Error()}
      buf, _ := json.Marshal(reserror)
      w.WriteHeader(err.Code)
      w.Write(buf)
      return
    }
    if res == nil {
      return
    }
    buf, _ := json.Marshal(res)
    w.Write(buf)
  }
}

