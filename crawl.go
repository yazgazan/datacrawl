
package main

import (
  "github.com/PuerkitoBio/goquery"
)

func isSelector(jsonReq map[string]interface{}) bool {
  selector, ok := jsonReq["_selector"]
  if ok == false {
    return false
  }
  _, ok = selector.(string)
  return ok
}

func isAttr(jsonReq map[string]interface{}) bool {
  /* if isSelector(jsonReq) == false { */
  /*   return false */
  /* } */
  attr, ok := jsonReq["_attr"]
  if ok == false {
    return false
  }
  _, ok = attr.(string)
  return ok
}

func crawl(doc *goquery.Selection,
jsonReq map[string]interface{}) (map[string]interface{}, *Error) {

  var err *Error;
  ret := make(map[string]interface{})

  for k, v := range(jsonReq) {
    if k == "_selector" || k == "_attr" {
      continue
    }
    switch v := v.(type) {
    default:
      return nil, &Error{
        M_unexpectedTime,
        500,
      }
    case string:
      ret[k], err = FetchTexts(doc, v)
      if err != nil {
        return nil, err
      }
    case map[string]interface{}:
      if isAttr(v) == true {
        ret[k], err = FetchAttr(doc, v)
        if err != nil {
          return nil, err
        }
      } else if isSelector(v) == true {
        ret[k], err = FetchSelector(doc, v)
        if err != nil {
          return nil, err
        }
      } else {
        ret[k], err = crawl(doc, v)
        if err != nil {
          return nil, err
        }
      }
    }
  }
  return ret, nil
}

