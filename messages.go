
package main

const (
  M_listening = "Listening on 'http://%s'"
  M_redirectApi = `<html>
Error, Command not found. Please check the api doc at
<a href="%s">%s
`
  M_unexpectedTime = "unexpected type in json"
  M_malformatedQuery = "malformated query"
  M_malformatedQuery_url = "malformated query (request)"
  M_malformatedQuery_req = "malformated query (request)"
  M_malformatedJson_req = "malformated json (request)"
  M_downloadFailed = "failed to download page"
)

