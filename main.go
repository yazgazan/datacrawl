
package main

import (
  "net/http"
  "os"
  "fmt"
)

func main () {
  listenOn := os.Getenv("LISTEN")
  if len(listenOn) == 0  {
    listenOn = D_listen
  }

  http.HandleFunc("/", errorHandler(DocRedirectCtrl))
  http.HandleFunc("/crawl", errorHandler(CrawlCtrl))
  fmt.Println(fmt.Sprintf(M_listening, listenOn))
  http.ListenAndServe(listenOn, nil)
}

