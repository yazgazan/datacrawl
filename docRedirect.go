
package main

import (
  "net/http"
  "fmt"
)

func DocRedirectCtrl(w http.ResponseWriter, r *http.Request) (Res, *Error) {
  w.WriteHeader(404)
  fmt.Fprintf(w, fmt.Sprintf(M_redirectApi, C_apiLink, C_apiLink))
  return nil, nil
}

