
package main

import (
  "github.com/PuerkitoBio/goquery"
)

func FetchTexts(doc *goquery.Selection, query string) ([]string, *Error) {
  elems := doc.Find(query)
  resArray := make([]string, elems.Size())
  i := 0
  elems.Each(func (_ int, elem *goquery.Selection) {
    resArray[i] = elem.Text()
    i++
  })
  return resArray, nil
}

func FetchAttr(doc *goquery.Selection, query map[string]interface{}) (string, *Error) {
  tmpdoc := doc
  selector, ok := query["_selector"].(string)
  if ok == true {
    tmpdoc = doc.Find(selector)
  }
  attr, _ := query["_attr"].(string)
  if tmpdoc.Size() == 0 {
    return "", nil
  }
  ret, _ := tmpdoc.Attr(attr)
  return ret, nil
}

func FetchSelector(doc *goquery.Selection,
jsonReq map[string]interface{}) ([]interface{}, *Error) {
  var err *Error = nil;

  selector, _ := jsonReq["_selector"].(string)
  elems := doc.Find(selector)
  retArray := make([]interface{}, elems.Size())
  i := 0
  elems.Each(func (_ int, elem *goquery.Selection) {
    if err != nil {
      return
    }
    retArray[i], err = crawl(elem, jsonReq)
    i++
  })
  return retArray, err
}

