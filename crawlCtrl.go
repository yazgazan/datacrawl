
package main

import (
  "github.com/PuerkitoBio/goquery"

  "net/http"
)

func CrawlCtrl(w http.ResponseWriter, r *http.Request) (Res, *Error) {
  var res ResCrawl;

  if err := r.ParseForm(); err != nil {
    return nil, &Error{
      M_malformatedQuery,
      400,
    }
  }
  url, err := FetchUrl(r)
  res.Url = url
  if err != nil {
    return nil, err
  }
  jsonReq, err := FetchJson(r)
  if err != nil {
    return nil, err
  }
  doc, er := goquery.NewDocument(url)
  if er != nil {
    return nil, &Error{
      M_downloadFailed,
      404,
    }
  }
  res.Results, err = crawl(doc.Selection, jsonReq)
  if err != nil {
    return nil, err
  }
  return res, nil
}

