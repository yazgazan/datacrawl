
package main

import (
  "encoding/json"
  "net/http"
)

func FetchJson(r *http.Request) (map[string]interface{}, *Error) {
  jsonstrs, ok := r.Form["request"]
  if ok == false || len(jsonstrs) != 1 {
    return nil, &Error{
      M_malformatedQuery_req,
      400,
    }
  }
  jsonstr := jsonstrs[0]
  jsonReq := make(map[string]interface{})
  err := json.Unmarshal([]byte(jsonstr), &jsonReq)
  if err != nil {
    return nil, &Error {
      M_malformatedJson_req,
      500,
    }
  }
  return jsonReq, nil
}

